import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";

const App = () => {
  const [classes, setClasses] = useState("circle");
  const [result, setResult] = useState(null);
  const [divide, setDivide] = useState(null);
  const [totalDeg, setTotalDeg] = useState("");
  const symbolSegments = {
    1: "12000",
    2: "11000",
    3: "9000",
    4: "7000",
    5: "6000",
    6: "8000",
    7: "0",
    8: "5000",
    9: "4000",
    10: "3000",
    11: "2000",
    12: "1000",
  };
  useState(() => {
    setTotalDeg(Math.floor(Math.random() * 10000) + 1);
  }, []);
  const handleWin = (actualDeg) => {
    const winningSymbolNr = Math.ceil(actualDeg / 30);
    setResult(symbolSegments[winningSymbolNr]);
  };
  const doSpin = () => {
    setClasses("circle start-rotating");

    let count = 0;
    let number = setInterval(() => {
      count++;
    }, 12);
    setTimeout(() => {
      clearInterval(number);
      setClasses("circle start-rotating pause-rotating");
      let test = count % 12;
      setDivide(test);
      const winningSymbolNr = Math.ceil(
        ((Math.floor(Math.random() * 10000) + 1) % 360) / 30
      );
      setResult(symbolSegments[winningSymbolNr]);
    }, totalDeg);
  };

  return (
    <div>
      <div className="arrow"></div>
      <ul className={classes}>
        <li>
          <div className="text">1000</div>
        </li>
        <li>
          <div className="text">2000</div>
        </li>
        <li>
          <div className="text">3000</div>
        </li>
        <li>
          <div className="text">4000</div>
        </li>
        <li>
          <div className="text">5000</div>
        </li>
        <li>
          <div className="text">0</div>
        </li>
        <li>
          <div className="text">8000</div>
        </li>
        <li>
          <div className="text">6000</div>
        </li>
        <li>
          <div className="text">7000</div>
        </li>
        <li>
          <div className="text">9000</div>
        </li>
        <li>
          <div className="text">11000</div>
        </li>
        <li>
          <div className="text">12000</div>
        </li>
      </ul>
      <div className="result_section">
        <button onClick={doSpin}>spin</button>
        <br></br>
        {result && <p>{result}</p>}
      </div>
    </div>
  );
};

export default App;
